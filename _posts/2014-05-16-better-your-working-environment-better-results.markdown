---
layout: post
title: Better your working environment = better results
date: 2014-05-16 19:12:48.000000000 +01:00
---

---

### Introduction

I've always found having a non-logical working environment has had an impact on not only motivation to work, but the quality of the work.

For example, I used to work with a very "flat" structure, and by this I mean; I had 2 versions of everything I did - live, and dev/test - which is logical, but quite "flat" and didn't take advantage of some *awesome* technologies and practices.

### My local environment

I've recently upgraded my local development environment from having a flat project structure, to a much more elegant structure - making use of VirtualHosts, and better naming conventions.

My set-up is as follows;

 * tools.localhost
 * clients.localhost
 * personal.localhost
 * playground.localhost
 * Version Control (git)
 
#### tools.localhost

   This is mainly used for various tools I need on a daily basis. It consists of some programs I've written, and some 3rd party ones.
   
#### clients.localhost
   This is the most active development section in my life. It consists and holds all client work I've done, and the ones in the pipeline.
   
#### personal.localhost
   This VirtualHost is for my personal projects and is as actively developed as clients.localhost.
   
#### playground.localhost
   This is my playground. If I want to mock up a quick script, just to test something, or just play around with new (to me) technologies, I do it here.
   
#### Version control
   Version control is the best, and every developer should use it. I personally use GIT, and I've deployed my own [GitLab](http://gitlabhq.com) server to hold my branches and code remotely.
   
#### The core set-up of my local environment
   For my live servers, I use machines running on CentOS 6.4, but locally, I use Windows. - I don't find any real concerns when pushing to live, considering different operating systems - everything runs smoothly.
   
   I use [XAMPP](https://www.apachefriends.org/index.html) on my Windows machine, and I've set-up the VirtualHosts mentioned above. Making use of the *aweomse* IDE - [NetBeans](https://netbeans.org/), development is pretty fun! (*Because NetBeans rules ;)*)
   
### Setting up the VirtualHosts
   Setting up the VirtualHosts was a little different (only slightly) than what I do when I set them up for my live servers.
   
   The VirtualHost entries are made `\xampp\apache\conf\extra\httpd-vhosts.conf` and are pretty much straight forward. 
   
   Once the VirtualHost's have been set-up, make sure you restart Apache.
   
```
<VirtualHost *:80>
ServerAdmin 	webmaster@tools.localhost
DocumentRoot 	"G:/xampp/htdocs/tools/"
ServerName 		tools.localhost
ErrorLog 		"logs/tools-error.log"
CustomLog		"logs/tools-access.log" common 
</VirtualHost>
```

*<small>Make sure you've uncommented `NameVirtualHost *:80` ;)</small>*    
      
I then had to make changes to `\xampp\apache\conf\httpd.conf`, to set `AllowOverride` to `All`, and create a `Directory` block to allow access.

So, that I've set-up my VirtualHosts to develop locally on, I have to edit my Windows Host file.

```
127.0.0.1		tools.localhost
127.0.0.1		playground.localhost
127.0.0.1		personal.localhost
127.0.0.1		clients.localhost
```

Then I just load up NetBeans, create a new project group for each VirtualHost, and manage my projects that way - it's *awesome*.


### Managing every project

To manage every project (I have about... 10?) I use GIT. It's superb.

As I mentioned above, I've [deployed](https://www.digitalocean.com/?refcode=b1c2c28c6822) my own GitLab server for centralized development. I use [a great branching model](http://nvie.com/posts/a-successful-git-branching-model/), and it's made development not only more enjoyable, but much more stable and elegant.

----

That's pretty much it for my development logic - I'd love to hear what you think about it below!

