---
layout: post
title: Emoji Rodeo - React to posts (like in Slack)
date: 2016-02-18 15:03:48.000000000 +01:00
---

---

### Emoji Rodeo

So, I've decided to add [emoji.rodeo](https://emoji.rodeo/) to my site! It's exactly like Slacks react feature - pretty fun!

> Imagine Slack's reactions had an adorable baby with Facebook's Like button that you could put on your own website. Well, imagine no more!

Below the title of each post, you'll see a `React` button, simply click it, then choose your reaction! :)

Nice
