---
layout: page
title: About
permalink: /about/
---

<div class="github-card" data-github="harrydenley" data-width="400" data-height="317" data-theme="medium"></div>
<script src="//cdn.jsdelivr.net/github-cards/latest/widget.js"></script>

I'm Harry Denley, born in June 1994.

**I use;**

 * PHP
 * MySQL
 * MongoDB

**I'm learning;**

 * [Go](https://golang.org/)

-----

#### Past stuff
I used to develop modules for a web game engine named [McCodes](http://mccodes.com) under the alias **[sniko](http://www.makewebgames.com/member.php/65371-sniko)**. I released quite a few modifications to the base that can be found [on MakeWebGames](https://www.google.co.uk/?gfe_rd=cr&ei=peRVVu_gDOLH8gfX5puAAw#q=%22sniko%22%2Binurl:makewebgames.com)

#### Now stuff
Now I'm developing some SaaS applications that hook into [Slack](http://slack.com) - which is pretty awesome. They're running on MongoDb, Go, and PHP over at [Heroku](https://heroku.com)

### Contact me

You can [Tweet me](http://twitter.com/sniko_).
